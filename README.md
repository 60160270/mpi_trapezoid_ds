* Run sequential.c by using ./a.out [a] [b] [n] *
        a is left point of x axis
        b is right point of y axis
        n is number of process
* Run parallel.c by using mpiexec --hostfile hostfile -np [process] mpi_trap b.out *
