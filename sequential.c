#include <stdio.h>
#include <stdlib.h>

double f(double x){
        return x*x;
}

int main(int argc, char* argv[])
{
	// 0 filname 1 a.input 2 b.input 3 n.input
	double approx,x_i;
	double a = atof(argv[1]);
	double b = atof(argv[2]);
	double n = atof(argv[3]);
	double h = (b-a)/n;	
	approx = (f(a) + f(b))/2.0;

	for(int i = 1; i <= n-1 ;i++){
		x_i = a + i * h;
		approx += f(x_i);
	} 
	approx = h * approx;
	
	printf("output approx of %lf to %lf = %lf",a,b,approx); 
	
	return 0;
}
